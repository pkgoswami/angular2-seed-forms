export class jobApplication{
	constructor (
		public firstName: string,
		public lastName: string,
		public isFullTime: boolean,
		public isContract: boolean,
		public payType: string,
		public primaryLanguage: string
	){

	}
}