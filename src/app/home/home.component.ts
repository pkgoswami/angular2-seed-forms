import {Component} from '@angular/core';
import {jobApplication} from '../models/jobapplication.model';
import { FormPoster } from '../services/form-poster-service';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent {
	//languages = ["English", "Spanish", "Other"];
	languages =[];
	model = new jobApplication("", "", true, false, "", "default");
	hasPrimaryLangError = false;

	constructor(private formPoster: FormPoster){
		this.formPoster.getLanguages()
			.subscribe(
				data => this.languages = data.languages,
				err => console.log('get error: ', err)
			);
	}
	validatePrimaryLang(value){
		if(value === "default"){
			this.hasPrimaryLangError = true;	
		}else{
			this.hasPrimaryLangError = false;
		}
	}

	submitForm(form: NgForm){
		this.validatePrimaryLang(this.model.primaryLanguage);
		if(this.hasPrimaryLangError)
			return;
		this.formPoster.postJobApplicationForm(this.model)
			.subscribe(
				data => console.log("success: ", data),
				err => console.log("error: ", err)				
			);
	}
	/*firstNameToUpperCase(value: string){
		if(value.length > 0) {
			this.model.firstName = value.charAt(0).toUpperCase() + value.slice(1);
		}else{
			this.model.firstName = value;
		}
	}*/
}
