import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { jobApplication } from '../models/jobapplication.model';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class FormPoster {
	constructor(private http: Http){

	}

	private extractData (res: Response){
		let body = res.json();
		return body.fields || { };
	}

	private extractLanguages (res: Response){
		let body = res.json();
		return body.data || { };
	}
	private handleError (error: any) {
		console.log("Post Error: ", error);
		return Observable.throw(error.statusText);
	}

	getLanguages() : Observable<any> {
		return this.http.get('http://localhost:3100/get-language')
						.delay(5000)
						.map(this.extractLanguages)
						.catch(this.handleError);
	}

	postJobApplicationForm(jobApplication: jobApplication):Observabel<any> {
		//console.log('Posting Job Application:', jobApplication);
		let body = JSON.stringify(jobApplication);
		let headers = new Headers({ 'Content-Type' : 'application/json' });
		let options = new RequestOptions({ headers: headers });

		return this.http.post('http://localhost:3100/postJobApplication', body, options)
						.map(this.extractData)
						.catch(this.handleError);
	}
}

